
export function getMouseEventPos(ev: MouseEvent): [number, number] {
    if (!ev.target || !(ev.target instanceof HTMLElement)) throw new Error("");
    var rect = ev.target.getBoundingClientRect()
    var x = ev.clientX - rect.left
    var y = ev.clientY - rect.top
    return [x, y]
}
