import { QNode, Quad, ROOT_QUAD_SIZE } from "./quadtree"

export const g_force_multiplier = 0.1

export interface GNode {
    label?: string
    id: string
    type: "room" | "area" | "person"
}
export interface GNodeI extends GNode, QNode {
    edges?: GEdge[]
    xv: number
    yv: number
}
export interface GEdge {
    label?: string
    id: string
    a_id?: string
    b_id?: string
}
export interface GEdgeI extends GEdge {
    a: GNodeI
    b: GNodeI
}

export class GraphVisualizer {
    edges: Map<string, GEdgeI> = new Map()
    nodes: Map<string, GNodeI> = new Map()

    quadtree: Quad

    canvas: HTMLCanvasElement
    c: CanvasRenderingContext2D

    constructor(nodes: GNode[], edges: GEdge[], canvas: HTMLCanvasElement) {
        this.canvas = canvas
        const c = canvas.getContext("2d")
        if (!c) throw new Error("sdfioashdf");
        this.c = c

        for (const n of nodes) {
            this.nodes.set(n.id, {
                x: Math.random() * 500,
                y: Math.random() * 500,
                xv: 0,
                yv: 0,
                ...n
            })
        }
        for (const e of edges) {
            if (!e.a_id || !e.b_id) throw new Error("dsajhfajskd");
            const a = this.nodes.get(e.a_id);
            const b = this.nodes.get(e.b_id);
            if (!a) throw new Error("cannot find " + e.a_id);
            if (!b) throw new Error("cannot find " + e.b_id);
            this.edges.set(e.id, {
                a, b,
                a_id: undefined,
                b_id: undefined,
                ...e
            })
        }

        this.quadtree = new Quad(ROOT_QUAD_SIZE, -ROOT_QUAD_SIZE / 2, -ROOT_QUAD_SIZE / 2)
    }

    sim_repell(a: GNodeI, b: GNodeI) {
        var d = Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))
        if (!d) d = Infinity
        var fac = -0.2 / Math.sqrt(d) + -100 / (d * d)
        this.sim_force(a, b, fac)
    }
    sim_attract(e: GEdgeI) {
        this.sim_force_raw(e.a, e.b, 0.01)
    }
    sim_force(na: GNodeI, nb: GNodeI, force: number) {
        var d = Math.sqrt((na.x - nb.x) * (na.x - nb.x) + (na.y - nb.y) * (na.y - nb.y))
        if (!d) d = Infinity
        this.sim_force_raw(na, nb, force / d)
    }
    sim_force_raw(na: GNodeI, nb: GNodeI, force: number) {
        var f_x = (na.x - nb.x) * force * g_force_multiplier
        var f_y = (na.y - nb.y) * force * g_force_multiplier
        na.xv -= f_x
        na.yv -= f_y
        nb.xv += f_x
        nb.yv += f_y
    }

    simulate() {

        // attract nodes that are connected
        this.edges.forEach(e => this.sim_attract(e))

        // repell nodes that are close together
        this.quadtree = new Quad(ROOT_QUAD_SIZE, -ROOT_QUAD_SIZE / 2, -ROOT_QUAD_SIZE / 2)
        this.quadtree.add(...this.nodes.values())
        this.nodes.forEach(n => {
            const quad_coll = 200
            this.quadtree.get(n.x - quad_coll, n.y - quad_coll, quad_coll * 2, quad_coll * 2).forEach(n2 => {
                // we can safely™ ignore this, as we only pass GNodeI to the quadtree
                //@ts-ignore
                this.sim_repell(n, n2)
            })
        })

        this.nodes.forEach(n => {
            n.x += n.xv
            n.y += n.yv
            n.xv *= 0.9
            n.yv *= 0.9
        })

    }

    draw(pan_x: number, pan_y: number, zoom: number) {
        // clear
        this.c.fillStyle = "black"
        this.c.fillRect(0, 0, this.canvas.width, this.canvas.height)

        // zoom
        this.c.save()
        this.c.translate(pan_x, pan_y);
        this.c.scale(zoom, zoom)

        // draw edges
        this.c.strokeStyle = "#ff00ff66";
        this.c.lineWidth = 2
        this.edges.forEach(e => this.draw_edge(e))

        // draw nodes
        this.c.fillStyle = "white";
        this.c.textAlign = "center"
        this.c.textBaseline = "middle"
        this.nodes.forEach(n => this.draw_node(n))

        if (false) this.quadtree.debugDraw(this.c)

        this.c.restore()
    }

    draw_edge(e: GEdgeI) {
        this.c.beginPath()
        this.c.moveTo(e.a.x, e.a.y)
        this.c.lineTo(e.b.x, e.b.y)
        this.c.stroke()
    }
    draw_node(n: GNodeI) {
        this.c.fillText(n.label ?? n.id, n.x, n.y)
    }

}
