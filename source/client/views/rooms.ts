import { GEdge, GNode, GraphVisualizer } from "../lib/graph";
import { getMouseEventPos } from "../lib/helper";
import { parse_thing_name } from "./names";
import { analyse_room_data, download_data } from "./room_data";

export interface Room {
    thing_count: number,
    things: string[]
}
export interface Data {
    rooms: Room[]
    people: string[]
}

var canvas: HTMLCanvasElement
var graph: GraphVisualizer

export async function v_rooms() {
    console.log("loading data");
    const data = await download_data()
    console.log("setting up canvas");

    setup_canvas()

    console.log("setting up graph visualizer");

    const { nodes, edges } = analyse_room_data(data)
    graph = new GraphVisualizer(nodes, edges, canvas)
    console.log("done");

    redraw()

}


let pan_x = 0, pan_y = 0, zoom = 1;
let pan_xv = 0, pan_yv = 0;
let debug = false;
let should_stop = false

function setup_canvas() {
    canvas = document.createElement("canvas")
    document.body.append(canvas)
    if (!canvas || !(canvas instanceof HTMLCanvasElement)) throw new Error("");
    const ctx = canvas.getContext("2d")
    if (!ctx) throw new Error("");
    const resize = () => {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    window.onresize = resize

    var drag_start: [number, number] | undefined, drag_start_pan: [number, number] | undefined;
    canvas.onmousedown = (ev) => {
        drag_start = getMouseEventPos(ev)
        drag_start_pan = [pan_x, pan_y]
    }
    canvas.onmouseup = () => {
        [drag_start, drag_start_pan] = [undefined, undefined]
    }
    canvas.onmousemove = (ev) => {
        if (drag_start && drag_start_pan) {
            var [mx, my] = getMouseEventPos(ev)
            pan_x = mx - drag_start[0] + drag_start_pan[0]
            pan_y = my - drag_start[1] + drag_start_pan[1]
        }
    }

    const zoomfn = (f: number) => {
        zoom *= f
        pan_x *= f
        pan_y *= f
    }

    window.onmousewheel = (ev: any) => {
        zoomfn(1 + ev.deltaY * -0.002)
    }
    window.onkeydown = (ev: KeyboardEvent) => {
        if (ev.repeat) return
        if (ev.key == "e") {
            zoomfn(0.9)
            ev.preventDefault()
        }
        if (ev.key == "q") {
            zoomfn(1.1)
            ev.preventDefault()
        }
        if (ev.key == "p") exportDownload()
        if (ev.key == "l") [pan_xv, pan_yv] = [0, 0]
        if (ev.key == "w") pan_yv += 1
        if (ev.key == "s") pan_yv -= 1
        if (ev.key == "a") pan_xv += 1
        if (ev.key == "d") pan_xv -= 1
        if (ev.key == "x") debug = !debug;
        if (ev.code == "Space") should_stop = true;
    }
    window.onkeyup = (ev: KeyboardEvent) => {
        if (ev.key == "w") pan_yv -= 1
        if (ev.key == "s") pan_yv += 1
        if (ev.key == "a") pan_xv -= 1
        if (ev.key == "d") pan_xv += 1
        ev.preventDefault()
    }

    resize()
}

function redraw() {
    graph.simulate()
    pan_x += pan_xv
    pan_y += pan_yv
    // console.log(pan_x, pan_y, zoom);
    graph.draw(pan_x, pan_y, zoom)
    // console.log(graph.nodes.get("Testing"));
    if (!should_stop)
        requestAnimationFrame(redraw)
}



export function exportDownload() {
    var exportObject = {
    }

    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(exportObject)));
    element.setAttribute('download', "dump.json");
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}
