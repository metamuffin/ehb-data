import { GEdge, GEdgeI, GNode } from "../lib/graph";
import { parse_room_name, parse_thing_name } from "./names";
import { Data } from "./rooms";

export async function download_data(): Promise<Data> {
    var data_req = await fetch("/data/output.json")
    if (!data_req.ok) throw new Error("Could not download data");
    return JSON.parse(await data_req.text())
}

export function analyse_room_data(data: Data): { edges: GEdge[], nodes: GNode[] } {
    // all rooms have a node
    let nodes: GNode[] = []
    // nodes will be edges between the rooms they connect
    let edges: GEdge[] = []

    let areas: Map<string, Set<string>> = new Map()
    let rooms: Set<string> = new Set()

    for (const [rname, r] of Object.entries(data.rooms)) {
        const info = parse_room_name(rname)
        nodes.push({
            id: rname,
            type: "room"
        })
        rooms.add(rname)
        if (info.area) {
            const a = areas.get(info.area) ?? new Set()
            a.add(info.room)
            areas.set(info.area, a)
        }
    }
    for (const [rname, r] of Object.entries(data.rooms)) {
        for (const t of r.things) {
            const info = parse_thing_name(rname, t);
            const area = areas.get(info.area ?? "dshfkljasdhf")
            if (!info.instance) continue
            if (!info.area) continue
            if (!info.room) continue

            if (info.instance.startsWith("DoorTo")) {
                console.log(`id: ${t}`);
                const target_name = info.instance.substr("DoorTo".length)
                console.log(`door from ${info.room} to ${target_name}`);
                const target = `${info.area}_${target_name}`
                if (rooms.has(target)) {
                    console.log(`linking ${rname} with ${target}`);
                    edges.push({
                        a_id: rname,
                        b_id: target,
                        id: t,
                    })
                }
            }
            if (rooms.has(info.instance)) {
                console.log(`linking sub-room ${t} to ${rname}`);

            }


        }
    }


    return { nodes, edges }
}