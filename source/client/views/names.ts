

export interface NameInfo {
    area?: string
    room: string
    instance?: string
    type?: string
    number?: string
}

// <area>_<room>_<type>_<instance>
// <name>
export function parse_thing_name(room: string, name: string): NameInfo {
    // remove leading room name
    if (name.startsWith(room + "_")) name = name.substr(room.length + 1)
    const parts = name.split("_")
    let type, number, instance;
    if (parts.length == 1) [instance] = parts
    if (parts.length == 2) [type, instance] = parts
    if (parts.length == 3) [type, instance, number] = parts
    if (!Number.isNaN(parseInt(instance ?? "a"))) number = instance, instance = undefined

    return {
        type, instance, number,
        ...parse_room_name(room)
    }
}


// <area>_<room>
export function parse_room_name(name: string): NameInfo {
    if (name.split("_").length != 2) return { room: name }
    let [area, room] = name.split("_")
    return { area, room }
}
