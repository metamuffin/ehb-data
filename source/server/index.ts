import Express, { static as estatic, json } from "express";
import { join } from "path";
import Webpack from "webpack"
import WebpackDevMiddleware from "webpack-dev-middleware"
import { existsSync, readFile, readFileSync } from "fs";
import http from "http"
import https from "https"
import expressWs from "express-ws";
import * as ws from "ws"

type Room = Map<string, ws>
const rooms: Map<string, Room> = new Map()


async function main() {
    const app_e = Express();
    const app = expressWs(app_e).app

    if (process.env.ENV == "production") {
        console.log("PRODUCTION MODE!!!");
        app.use("/scripts", estatic(join(__dirname, "../../public/dist")))
    } else {
        console.log("DEVELOPMENT MODE!!!");
        const webpackConfig = require('../../webpack.dev');
        const compiler = Webpack(webpackConfig)
        const devMiddleware = WebpackDevMiddleware(compiler, {
            publicPath: webpackConfig.output.publicPath
        })
        app.use("/scripts", devMiddleware)
    }

    app.disable("x-powered-by");
    app.use(json());

    app.get("/", (req, res) => {
        res.sendFile(join(__dirname, "../../public/index.html"));
    });

    app.use("/", estatic(join(__dirname, "../../public")));

    app.use((req, res, next) => {
        res.status(404);
        res.send("This is an error page");
    });

    const port = parseInt(process.env.PORT ?? "8080")
    app.listen(port, process.env.HOST ?? "127.0.0.1", () => {
        console.log(`Server listening on ${process.env.HOST ?? "127.0.0.1"}:${port}`);
    })
}

main();