import { Data } from "../client/views/rooms";
import { readFileSync } from "fs";
import { join } from "path";



const data: Data = JSON.parse(readFileSync(join(__dirname, "../../public/data/output.json")).toString())

const useful_points = []
for (const [rname, r] of Object.entries(data.rooms)) {
    for (const t of r.things) {
        if (t.search("Point") != -1) {
            useful_points.push(t)
            break;
        }
    }
}
let combinations: [string, string][] = []
for (const p1 of useful_points) {
    for (const p2 of useful_points) {
        combinations.push([p1, p2])
    }
}
let selected_combinations: [string, string][] = []
while (selected_combinations.length < 10) {
    const i = Math.floor(Math.random() * combinations.length)
    selected_combinations.push(combinations[i])
    combinations.splice(i, 1)
}


let code = `
void sp(string p1,string p2)
    string a = ""
    loop d in FindPath(p1,p2)
        a += d + ","
    end
    SaveData(p1 + ";" + p2 + ";" + a)
end
`
for (let i = 0; i < selected_combinations.length; i++) {
    const [p1, p2] = selected_combinations[i];
    code += `Print("${i}/${selected_combinations.length}: ${p1.substring(0, 10)} -> ${p2.substring(0, 10)}")\n`
    code += `sp("${p1}","${p2}")\n`
}
console.log(code);

